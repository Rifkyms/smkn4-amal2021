$('#sidebarCollapse').on('click', function () {
  $('#sidebar, #content').toggleClass('active')
})

$(document).ready(function () {
  if ($('#home').hasClass('active')) {
    $('.icon-sidebar-home').css('color', 'white')
    $('.link-home').css('color', 'white')
  } else if ($('#statistics').hasClass('active')) {
    $('.icon-sidebar-statistics').css('color', 'white')
    $('.link-statistics').css('color', 'white')
  } else if ($('#company').hasClass('active')) {
    $('.icon-sidebar-company').css('color', 'white')
    $('.link-company').css('color', 'white')
  } else if ($('#profile').hasClass('active')) {
    $('.icon-sidebar-user').css('color', 'white')
    $('.link-profile').css('color', 'white')
  } else if ($('#policy').hasClass('active')) {
    $('.icon-sidebar-policy').css('color', 'white')
    $('.link-policy').css('color', 'white')
  } else if ($('#task').hasClass('active')) {
    $('.icon-sidebar-task').css('color', 'white')
    $('.link-task').css('color', 'white')
  }

  $('#table-provinsi').dataTable()
  $('#table-kota').dataTable()
  $('#table-kelurahan').dataTable()
})

// script for statistic
$('.bars li .bar').each(function (key, bar) {
  const percentages = $(this).data('percentage')
  $(this).animate({
    height: percentages + '%'
  }, 1000)
})

// script for sub menu sidebar
function myFunction () {
  myVar = setTimeout(3000)
}

$('.kelola-btn').click(function () {
  $('nav ul li .kelola-show').toggleClass('show', 3000)
})
$('.laporan-btn').click(function () {
  $('nav ul li .laporan-show').toggleClass('show1', 3000)
})
$('.tabel-btn').click(function () {
  $('nav ul li .tabel-show').toggleClass('show2', 3000)
})

// To do list
$(document).ready(function () {
  //  main button click function
  $('button#create-task').on('click', function () {
    let task = $('input[name=task-insert]').val()
		
		if (task.length == 0) {

    } else if (task.length > 0) {
      // remove nothing message
      if ('.nothing-message') {
        $('.nothing-message').hide('slide', { direction: 'left' }, 300)
      };

      // create the new li from the form input

      let newTask = '<li class="task-li">' + '<p>' + task + '</p>' + '</li>'
      $('#task-list').append(newTask)

		// clear form when button is pressed
		$('input').val('')
		

		// makes other controls fade in when first task is created
		$('#controls').fadeIn()
		$('.task-headline').fadeIn()
		}
  })

  // mark as complete
  $(document).on('click', 'li', function () {
    $(this).toggleClass('complete')
  })

  // double click to remove
  $(document).on('dblclick', 'li', function () {
    $('.task-li').remove()
  })

  // Clear all tasks button
  $('button#clear-all-tasks').on('click', function () {
    $('#task-list li').remove()
    $('.task-headline').fadeOut()
    $('#controls').fadeOut()
    $('.nothing-message').show('fast')
  })
})
