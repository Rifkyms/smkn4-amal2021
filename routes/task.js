const express = require('express')
const router = express.Router()

router.get('/', function (req, res) {
  const linkTask = req.params
  res.render('task/task', {
    linkTask: linkTask,
    title: 'Task'
  })
})

module.exports = router
