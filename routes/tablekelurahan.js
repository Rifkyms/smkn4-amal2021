const express = require('express')
const router = express.Router()
const functionAPI = require('../lib/api')

router.get('/', function (req, res) {
  res.render('datatable/dataKelurahan', {})
})

router.get('/:idKecamatan', (req, res) => {
  const linkTable = req.params
  const idKelurahan = req.params.idKecamatan
  const reqDataApi = {
    url: `/api/daerahindonesia/kelurahan?id_kecamatan=${idKelurahan}`
  }
  functionAPI.reqDataApi(reqDataApi, (response) => {
    res.render('datatable/dataKelurahan', {
      data: response.data.kelurahan,
      linkTableKecamatan: linkTable,
      title: 'List Sub District'
    })
  })
})

router.get('/detail/:idKelurahan', (req, res) => {
  const idKelurahan = req.params.idKelurahan
  const reqDataApi = {
    url: `/api/daerahindonesia/kelurahan/${idKelurahan}`
  }
  functionAPI.reqDataApi(reqDataApi, (response) => {
    res.send({ data: response.data })
  })
})

router.get('/dataDetail/:idDetailKelurahan', (req, res) => {
  const idDetailKelurahan = req.params.idDetailKelurahan
  const reqDataApi = {
    url: `/api/daerahindonesia/kelurahan/${idDetailKelurahan}`
  }
  functionAPI.reqDataApi(reqDataApi, (response) => {
    res.send({ data: response.data })
  })
})

module.exports = router
