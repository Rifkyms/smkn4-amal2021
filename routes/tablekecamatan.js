const express = require('express')
const router = express.Router()
const functionAPI = require('../lib/api')

router.get('/', function (req, res) {
  res.render('datatable/dataKecamatan', {
  })
})

router.get('/:idKota', (req, res) => {
  const linkTable = req.params
  const idKecamatan = req.params.idKota
  const reqDataApi = {
    url: `/api/daerahindonesia/kecamatan?id_kota=${idKecamatan}`
  }
  functionAPI.reqDataApi(reqDataApi, (response) => {
    res.render('datatable/dataKecamatan', {
      data: response.data.kecamatan,
      linkTableKecamatan: linkTable,
      title: 'List District'
    })
  })
})

router.get('/detail/:idKecamatan', (req, res) => {
  const idKecamatan = req.params.idKecamatan
  const reqDataApi = {
    url: `/api/daerahindonesia/kecamatan/${idKecamatan}`
  }
  functionAPI.reqDataApi(reqDataApi, (response) => {
    res.send({ data: response.data, title: 'List District' })
  })
})

router.get('/kelurahan/:idKecamatan', (req, res) => {
  const idKecamatan = req.params.idKecamatan
  const reqDataApi = {
    url: `/api/daerahindonesia/kelurahan/?id_kecamatan=${idKecamatan}`
  }
  functionAPI.reqDataApi(reqDataApi, (response) => {
    res.send({ data: response.data.kelurahan, title: 'List District' })
  })
})

module.exports = router
