const express = require('express')
const router = express.Router()

router.get('/', function (req, res) {
  const linkCompany = req.params
  res.render('companyProfile/companyProfile', {
    linkCompany: linkCompany,
    title: 'Company Profile'
  })
})

module.exports = router
