const express = require('express')
const router = express.Router()

const nameUser = 'Rafi Khoirulloh'
const titleUser = 'Web Developer'
const deskripsi = 'My name is Rafi Khoirulloh, born in Klaten, Central Java on July 1, 2004. I have a hobby, I like learning programming and playing games. My home address is on Jl. Babakan Jati Mulya RT 01 Rw 07 Gumuruh Village, Batununggal Subdistrict, Postal Code 40275'
const tags = ['Technology', 'Information', 'Profile']
const country = 'Bandung,Indonesia'
const bahasa = 'Indonesian, English'
const username = 'Baymax'
const linkImg = 'https://emojipedia-us.s3.dualstack.us-west-1.amazonaws.com/thumbs/120/lg/57/flag-for-indonesia_1f1ee-1f1e9.png'

router.get('/', function (req, res) {
  const linkProfile = req.params
  res.render('infoProfile/infoProfile', {
    nameUser: nameUser,
    titleUser: titleUser,
    deskripsi: deskripsi,
    tags: tags,
    country: country,
    bahasa: bahasa,
    username: username,
    linkImg: linkImg,
    linkProfile: linkProfile,
    title: 'Info Profile'
  })
})

module.exports = router
