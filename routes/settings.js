const express = require('express')
const router = express.Router()

router.get('/', function (req, res) {
  const linkSettings = req.params
  res.render('settings/settings', {
    linkSettings: linkSettings,
    title: 'Edit Profile'
  })
})

module.exports = router