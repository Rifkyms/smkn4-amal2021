const express = require('express')
const router = express.Router()

router.get('/', function (req, res) {
  const linkstatisticsIncome = req.params
  res.render('statistics/statisticsIncome', {
    linkstatisticsIncome: linkstatisticsIncome,
    title: 'Statistics Income'
  })
})

module.exports = router
