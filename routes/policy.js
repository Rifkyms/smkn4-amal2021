const express = require('express')
const router = express.Router()

router.get('/', function (req, res) {
  const linkPolicy = req.params
  res.render('policy/policy', {
    linkPolicy: linkPolicy,
    title: 'Policy'
  })
})

module.exports = router
