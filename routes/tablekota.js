const express = require('express')
const router = express.Router()
const axios = require('axios')

router.get('/', function (req, res) {
  res.render('datatable/dataKota')
})

router.get('/:idProvinsi', (req, res) => {
  const linkTable = req.params
  const idKota = req.params.idProvinsi
  axios.get(`https://dev.farizdotid.com/api/daerahindonesia/kota?id_provinsi=${idKota}`)
    .then((response) => {
      res.render('datatable/dataKota', {
        data: response.data.kota_kabupaten,
        linkTableKota: linkTable,
        title: 'List City'
      })
    })
    .catch((error) => {
      res.status(404).json({
        message: error.message
      })
    })
})

router.get('/kota/:id', (req, res) => {
  const idKota = req.params.id
  axios.get(`https://dev.farizdotid.com/api/daerahindonesia/kota/${idKota}`)
    .then((response) => {
      res.send({ data: response.data })
    })
    .catch((error) => {
      res.status(404).json({
        message: error.message
      })
    })
})

router.get('/kecamatan/:idKota', (req, res) => {
  const idKota = req.params.idKota
  axios.get(`https://dev.farizdotid.com/api/daerahindonesia/kecamatan?id_kota=${idKota}`)
    .then((response) => {
      res.send({ data: response.data.kecamatan, title: 'List District' })
    })
    .catch((error) => {
      res.status(404).json({
        message: error.message
      })
    })
})

module.exports = router
