const express = require('express')
const router = express.Router()

router.get('/', function (req, res) {
  const linkStatistics = req.params
  const data = ['100', '80', '60', '40', '20', '0']
  const datapercentage = ['75', '50', '60', '45', '65']
  const nameMonthly = ['Jan', 'Feb', 'Mar', 'Apr', 'May']
  res.render('statistics/statistics', {
    data: data,
    datapercentage: datapercentage,
    nameMonthly: nameMonthly,
    linkStatistics: linkStatistics,
    title: 'Statistics Market'
  })
})

module.exports = router