const express = require('express')
const router = express.Router()

router.get('/', function (req, res) {
  const linkstatisticsExpenses = req.params
  res.render('statistics/statisticsExpenses', {
    linkstatisticsExpenses: linkstatisticsExpenses,
    title: 'Statistics Expenses'
  })
})

module.exports = router