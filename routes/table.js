const express = require('express')
const router = express.Router()
const functionAPI = require('../lib/api')

router.get('/', function (req, res) {
  const linkTable = req.params
  const reqDataApi = {
    url: '/api/daerahindonesia/provinsi'
  }
  functionAPI.reqDataApi(reqDataApi, (response) => {
    res.render('datatable/datatable', {
      data: response.data.provinsi,
      linkTableProvinsi: linkTable,
      title: 'List Provinsi'
    })
  })
})

router.get('/:id', (req, res) => {
  const id = req.params.id
  const reqDataApi = {
    url: `/api/daerahindonesia/provinsi/${id}`
  }
  functionAPI.reqDataApi(reqDataApi, (response) => {
    res.send({ data: response.data })
  })
})

router.get('/kota/:idprovinsi', (req, res) => {
  const id = req.params.idprovinsi
  const reqDataApi = {
    url: `/api/daerahindonesia/kota?id_provinsi=${id}`
  }
  functionAPI.reqDataApi(reqDataApi, (response) => {
    res.send(response.data.kota_kabupaten)
  })
})

router.get('/:id', (req, res) => {
  const id = req.params.id
  const reqDataApi = {
    url: `/api/daerahindonesia/provinsi/${id}`
  }
  functionAPI.reqDataApi(reqDataApi, (response) => {
    res.send({ data: response.data })
  })
})

router.get('/modalcuaca/:namaProvinsi', (req, res) => {
  const namaProvinsi = req.params.namaProvinsi
  const reqDataApiCuaca = {
    url: `/?menu=cuaca&wilayah=${namaProvinsi}`
  }
  functionAPI.reqDataApiCuaca(reqDataApiCuaca, (response) => {
    res.send({ data: response.data })
  })
})

module.exports = router
