const express = require('express')
const path = require('path')
const bodyParser = require('body-parser')
const fileUpload = require('express-fileupload')
const favicon = require('serve-favicon')
const cookieParser = require('cookie-parser')
const helmet = require('helmet')
const morgan = require('morgan')

const app = express()

// view engine setup
app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'pug')
app.use(fileUpload())
app.use(helmet.frameguard())
app.use(helmet.xssFilter())
app.use(helmet.noSniff())
app.disable('x-powered-by')
app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')))
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({
  extended: true
}))
app.use(cookieParser())
app.use(express.static(path.join(__dirname, 'public')))

// Routing
app.use('/home', require('./routes/dashboard'))
app.use('/table', require('./routes/table'))
app.use('/listkota', require('./routes/tablekota'))
app.use('/listkecamatan', require('./routes/tablekecamatan'))
app.use('/listkelurahan', require('./routes/tablekelurahan'))
app.use('/statistics', require('./routes/statistics'))
app.use('/infoProfile', require('./routes/infoProfile'))
app.use('/companyProfile', require('./routes/companyProfile'))
app.use('/login', require('./routes/login'))
app.use('/policy', require('./routes/policy'))
app.use('/task', require('./routes/task'))
app.use('/settings', require('./routes/settings'))
app.use('/statisticsIncome', require('./routes/statisticsIncome'))
app.use('/statisticsExpenses', require('./routes/statisticsExpenses'))

app.get('/', (req, res) => {
  res.render('login/login')
})

// Logger
app.use(morgan('tiny'))

app.use((err, res, next) => {
  if (err) {
    res.render('404')
  }
})

app.listen(3000, function () {
  console.log('Server started on http://localhost:3000')
})

module.exports = app
