const axios = require('axios')

exports.reqDataApi = (dataApi, callback, fallout) => {
  axios.get('https://dev.farizdotid.com' + dataApi.url)
    .then((response) => {
      callback(response)
    })
    .catch((error) => {
      callback(error)
    })
}

exports.reqDataApiCuaca = (dataApiCuaca, callback, fallout) => {
  axios.get('http://iotcampus.net/bmkg' + dataApiCuaca.url)
    .then((response) => {
      callback(response)
    })
    .catch((error) => {
      callback(error)
    })
}
